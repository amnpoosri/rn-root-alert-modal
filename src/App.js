/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';

import Modal from 'react-native-modal';
import {Manager as ModalManager} from 'react-native-root-modal';
import AlertModal from './components/AlertModal';

type Props = {};
export default class App extends Component<Props> {

  showAlert = () => {

    // Create a Modal element on screen.
let modal = new ModalManager(
          <AlertModal
        isVisible={true}
          onPress={() => {
            modal.destroy();
          }}
          onCancel={() => {
            modal.destroy();
          }}
          description={'asdasds'}
          submitText={'OK'}
          title={'ljk;ljkj'} />
);

  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}
          onPress={this.showAlert}>Welcome to React Native!
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
