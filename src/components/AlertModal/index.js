import React from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';
import PropTypes from 'prop-types';
import Modal from 'react-native-modal';

import RoundedButton from '../RoundedButton';

const AlertModal = (props) => {
  return (
    <Modal
      isVisible={props.isVisible}
    >
      <View style={styles.container}>
        <Text style={styles.titleText}>{props.title}</Text>
        <Text style={styles.descriptionText}>{props.description}</Text>
        <View style={styles.buttonGroup}>
          {(typeof props.onCancel !== 'undefined') && <View style={styles.buttonWrapper}>
            <RoundedButton
              onPress={props.onCancel}
              style={{backgroundColor: '#e76349'}}
              text={'cancel'}
            />
          </View>}
          <View style={styles.buttonWrapper}>
            <RoundedButton
              onPress={props.onPress}
              style={{backgroundColor: '#82c44b'}}
              text={'ok'}
            />
          </View>
        </View>
      </View>
    </Modal>
  );
};
AlertModal.propTypes = {
  isVisible: PropTypes.bool.isRequired,
  title: PropTypes.string,
  description: PropTypes.string,
  onPress: PropTypes.func.isRequired,
  onCancel: PropTypes.func
};
const styles = StyleSheet.create({
  container: {
    borderRadius: 10,
    backgroundColor: 'white',
    paddingVertical: 24,
    paddingHorizontal: 12
  },
  titleText: {
    color: 'black',
    fontSize: 18,
    fontWeight: 'bold',
    paddingVertical: 15,
    paddingHorizontal: 10,
    alignSelf: 'center'
  },
  descriptionText: {
    color: 'grey',
    paddingVertical: 10,
    marginBottom: 10,
    textAlign: 'center'
  },
  buttonGroup: {
    flexDirection: 'row'
  },
  buttonWrapper: {
    flex: 1,
    paddingHorizontal: 5
  }
});

export default AlertModal;
