import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, View, Text, StyleSheet} from 'react-native';

class RoundedButton extends Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    textColor: PropTypes.string,
    textSize: PropTypes.number,
    onPress: PropTypes.func,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array
    ]),
    icon: PropTypes.element,
    styleIcon: PropTypes.object,
    disable: PropTypes.bool
  };

  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress}
        disabled={this.props.disabled}
        activeOpacity={0.9}>
        <View style={[styles.roundButton, this.props.style]}>
          { this.props.icon &&
        <View style={[styles.icon, this.props.styleIcon]}>
          {this.props.icon}
        </View>
          }
          <Text style={[
              styles.roundButtonText, 
              {color: this.props.textColor ? this.props.textColor : 'white', 
              fontSize: this.props.textSize? this.props.textSize: 16}]}>{this.props.text.toUpperCase()}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

RoundedButton.defaultProps = {
  text: '',
  onPress: () => {},
  style: {
    backgroundColor: 'red'
  }
};

const styles = StyleSheet.create({
  icon: {
    alignSelf: 'center',
    backgroundColor: 'transparent',
    marginRight: 10
  },
  roundButton: {
    justifyContent: 'center',
    borderRadius: 100,
    alignSelf: 'stretch',
    flexDirection: 'row',
    padding: 5,
    height: 47,
    backgroundColor: 'red',
    marginHorizontal: 10
  },
  roundButtonText: {
    alignSelf: 'center',
    textAlign: 'center',
    color: 'white',
    fontWeight: '900',
    fontSize: 16
  }
});

export default RoundedButton;
